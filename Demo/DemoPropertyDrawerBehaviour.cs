// Copyright (c) Slipp Douglas Thompson and Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using System;
using System.Collections.Generic;
using UnityEngine;
using Rotorz.ReorderableList;

public class DemoPropertyDrawerBehaviour : MonoBehaviour {
	
	[ReorderableList("list")]
	public PointsWrapper points = new List<Vector2>();
	
	[Serializable] public struct PointsWrapper {
		public List<Vector2> list;
		public static implicit operator List<Vector2>(PointsWrapper c) { return c.list; }
		public static implicit operator PointsWrapper(List<Vector2> l) { return new PointsWrapper(){ list = l }; }
	}
}