// Copyright (c) Slipp Douglas Thompson and Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using System;
using UnityEngine;
using Rotorz.ReorderableList;

namespace Rotorz.ReorderableList {
	
	/// <summary>
	/// Attribute which may be used to draw the List with <see cref="ReorderableListDrawer"/>.
	/// </summary>
	/// <example>
	/// <para>The attribute must be placed on a field with the type of struct/class containing a List field:</para>
	/// <code language="csharp"><![CDATA[
	/// [Serializable] public struct ThingsContainer {
	/// 	public List<string> list;
	/// }
	/// 
	/// [ReorderableList("list")] public ThingsContainer things;
	/// ]]></code>
	/// </example>
	public class ReorderableListAttribute : PropertyAttribute
	{
		/// <summary>
		/// The name of the contained field that is the List.
		/// </summary>
		public string listFieldName { get; private set; }
		
		/// <summary>
		/// Flags that'll be passed onto ReorderableListGUI.
		/// </summary>
		public ReorderableListFlags flags { get; private set; }
		
		
		/// <summary>
		/// Initializes a new instance of <see cref="ReorderableListAttribute"/>.
		/// </summary>
		/// <param name="listFieldName">Name of List field.</param>
		/// <param name="flags">Flags for use by <see cref="ReorderableListGUI"/>.</param>
		public ReorderableListAttribute(string listFieldName, ReorderableListFlags flags=0)
		{
			this.listFieldName = listFieldName;
			this.flags = flags;
		}
	}
	
}
