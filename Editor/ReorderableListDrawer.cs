// Copyright (c) Slipp Douglas Thompson and Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using System;
using UnityEngine;
using UnityEditor;
using Rotorz.ReorderableList;

namespace Rotorz.ReorderableList {
	
	/// <summary>
	/// Drawer for use with <see cref="ReorderableListAttribute"/>.
	/// </summary>
	[CustomPropertyDrawer(typeof(ReorderableListAttribute))]
	public class ReorderableListDrawer : PropertyDrawer
	{
		/// <summary>
		/// Helper to typecast PropertyDrawer's attribute property to the expected type: <see cref="ReorderableListAttribute"/>.
		/// </summary>
		public ReorderableListAttribute reorderableListAttribute {
			get { return (ReorderableListAttribute)this.attribute; }
		}
		
		/// <summary>
		/// Standard Unity GUI draw-phase hook; draws the GUI via ReorderableListGUI.Title() and ReorderableListGUI.ListFieldAbsolute().
		/// </summary>
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);
			
			SerializedProperty listProperty = GetListProperty(property);
			
			float titleHeight = GetTitleHeight(label);
			Rect titlePosition = position; titlePosition.height = titleHeight;
			Rect listPosition = position; listPosition.y += (titleHeight - 1f); listPosition.height -= titleHeight;
			
			ReorderableListGUI.Title(titlePosition, label);
			ReorderableListGUI.ListFieldAbsolute(listPosition, listProperty, this.reorderableListAttribute.flags);
			
			EditorGUI.EndProperty();
		}
		
		/// <summary>
		/// Standard Unity GUI layout-phase hook; computes the height via GetTitleHeight() and ReorderableListGUI.CalculateListFieldHeight().
		/// </summary>
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			SerializedProperty listProperty = GetListProperty(property);
			
			float titleHeight = GetTitleHeight(label);
			float listHeight = ReorderableListGUI.CalculateListFieldHeight(listProperty, this.reorderableListAttribute.flags);
			return titleHeight + listHeight;
		}
		
		/// <summary>
		/// Gets the sub-field's "listProperty" and ensures it isn't null.
		/// </summary>
		SerializedProperty GetListProperty(SerializedProperty property)
		{
			SerializedProperty listProperty = property.FindPropertyRelative(this.reorderableListAttribute.listFieldName);
			if (listProperty == null)
				throw new ArgumentException("Unable to find a list sub-field with name '"+this.reorderableListAttribute.listFieldName+"'.", "attribute.listFieldName");
			
			return listProperty;
		}
		
		/// <summary>
		/// Calculates the height needed to layout the titleLabel.
		/// </summary>
		public static float GetTitleHeight(GUIContent titleLabel)
		{
			return ReorderableListGUI.defaultTitleStyle.CalcHeight(titleLabel, 0f) + 6f;
		}
	}
	
}
